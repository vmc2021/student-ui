import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Student } from '../models/Student';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  // what is the  URl in the backend
  // http://localhost:8080/api/v1/students

  private getUrl: string = "http://localhost:8080/api/v1/students";

  // using dependency injection, bring in the HttpClient
  constructor(private httpClient: HttpClient) { }

  // method that will make a request to the GetMapping method in our backend
  getStudents(): Observable<Student[]> {
    return this.httpClient.get<Student[]>(this.getUrl).pipe(
      map(result => result)
    )
  }

  // method that will make a request to the PostMapping method in our backend
  saveStudent(newStudent: Student): Observable<Student> {
    return this.httpClient.post<Student>(this.getUrl, newStudent);
  }

   // Feature 3 frontend after finishing backend
  // method that will make a request to the GetMapping method
  // to view an individual Post from our backend
  viewStudent(id: number): Observable<Student> {
    return this.httpClient.get<Student>(`${this.getUrl}/${id}`).pipe(
      map(result => result)
    )
  }

   // Feature 4
  // method that will make a request to the DeleteMapping method
  deleteStudent(id: number): Observable<any> {
    return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'})
  }




}
