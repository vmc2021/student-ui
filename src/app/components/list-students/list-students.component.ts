import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/models/Student';
import { StudentService } from 'src/app/services/student.service';

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.css']
})
export class ListStudentsComponent implements OnInit {
  // property
  students: Student[] = [];

  // bring in our service, create private variable
  constructor(private studentSvc: StudentService) { }

  ngOnInit(): void {
    this.listStudents();
  }// end of ngOnInit------------------------------------------

  // create an observable method that will display the list of our students / data
  listStudents(){
    this.studentSvc.getStudents().subscribe(
      data => this.students = data
    )
  } 

    // method that will delete the data using the deletePost() method from our service
    deletedStudent(id: number) {
      this.studentSvc.deleteStudent(id).subscribe(
        data => this.listStudents()
      )
    }
  


}// end of class-----------------------------------------------
